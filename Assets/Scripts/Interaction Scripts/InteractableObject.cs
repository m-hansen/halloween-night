﻿using UnityEngine;
using System.Collections;

public abstract class InteractableObject : MonoBehaviour{

	public InteractableObject(){

	}

	public abstract void Activate();

}